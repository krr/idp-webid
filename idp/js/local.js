var myLayout;
window.addEventListener('HTMLImportsLoaded', function(e) {

    myLayout = $('#ideContainer').layout({
        closable: true, // pane can open & close
        resizable: true, // when open, pane can be resized
        slidable: true, // when closed, pane can 'slide' open over other panes - closes on mouse-out
        livePaneResizing: true,
        east__minSize: 100,
        center__minWidth: 100,
        east__size: .4,
        enableCursorHotkey: false
    });
});

function isUpToDate() {
    if ($("#filename").val() === ""){
        return true;
    }

    var up = false;

    $.ajax({
      type: 'POST',
      url: "./getFile",
      data: $("#filename").val(),
      success: function(res){
        if (res === getCode()){
            up = true;
        }
      },
      async:false
    });
    return up;
}

function loadPicker(e) {
    if (!isUpToDate() && !confirm("You have unsaved changes, if you continue your current work may be lost")) {
        return;
    }
    loadExample(e);
}

function loadExample(e) {
    $.post("./getFile", e)
        .done(function(d) {
            $("#filename").val(e);
            window.editor.mirror.setValue(d);
            $("#fileStatus").removeClass("glyphicon-flash");
            $("#fileStatus").addClass("glyphicon-ok");
        });
}

function save(e) {
    var json = {
        "path": $("#filename").val(),
        "code": getCode()
    }

    $.post("./saveFile", JSON.stringify(json)).done(function(e) {
        $("#fileStatus").removeClass("glyphicon-flash");
        $("#fileStatus").addClass("glyphicon-ok");
    });
    fileTreeInit();

}

function fileTreeInit() {
    $('#examples').fileTree({
        script: './listFiles',
        collapseSpeed: 100,
        expandSpeed: 100,
        root: ""
    }, function(file) {
        loadPicker(file)
        $("#picker").click();
    });
}


function setSize() {
    var containerHeight = $(window).height();
    $("#ideContainer").height(containerHeight - 70);
    $(".CodeMirror").height(containerHeight - 150);
    $(".jquery-console-inner").height(containerHeight - 130);

    $("#rightpart").height(containerHeight - 150);
    mh = (window.innerHeight - 100) + "px";
    $("#examples").css({
        "max-height": mh
    });
}

function killIDP() {
    document.socket.emit("abort", {})
}


window.addEventListener('HTMLImportsLoaded', function(e) {
    //INIT EDITOR
    $(window).resize(function() {
        setSize();
    });

    fileTreeInit();




    var te = document.getElementById("codeArea");
    var uiOptions = {
        path: 'js/',
        searchMode: 'popup'
    }
    var codeMirrorOptions = {
        mode: "idp",
        matchBrackets: true,
        lineNumbers: true,
        lineWrapping: true,
        extraKeys: {
            "Ctrl-Q": function(cm) {
                cm.foldCode(cm.getCursor());
            },
            "Ctrl-Space": "autocomplete",
            "Ctrl-Enter": function() {
                run();
            },
            "Ctrl-R": function() {
                run();
            },
            "Shift-Ctrl-D": function() {
                killIDP();
            },
            "Ctrl-S": function() {
                save();
            },
            "Ctrl-E": function() {
                saveGist();
            },
            "Esc": function(cm) {
                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            },
            "Shift-Ctrl-F": function() {
                window.editor.reindent();
            },
            "Alt-C": function() {
                $("#replSymb").click();
            }
        },
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "core-gutter", "CodeMirror-foldgutter", "CodeMirror-lint-markers"],
        lint: true,
        theme: "elegant",
        styleActiveLine: true,
        autoCloseBrackets: true
    };



    window.editor = new CodeMirrorUI(te, uiOptions, codeMirrorOptions);
    window.editor.options.lint = true;
    window.editor.mirror.state.lint.options.async = true;
    window.editor.mirror.on("change", function() {
        $("#copylink").text("");
        $("#fileStatus").removeClass("glyphicon-ok");
        $("#fileStatus").addClass("glyphicon-flash");
        localStorage.code = getCode();
        if ($("#filename").val()) {
            localStorage.filename = $("#filename").val();
        }
    });


    var di = $('<div>', {
        class: "input-group input-group-sm",
        id: "filePickerDiv"
    });
    di.append($('<span>', {
        class: "glyphicon input-group-addon",
        id: "fileStatus"
    }));
    di.append($('<input>', {
        type: "text",
        class: "form-control",
        placeholder: "Pick a file or write a new filename here",
        id: "filename"
    }));
    $("#buttonList").append(di);


    $("#filename").keypress(function(e) {
        $("#fileStatus").removeClass("glyphicon-ok");
        $("#fileStatus").addClass("glyphicon-flash");
        localStorage.code = getCode();
        if ($("#filename").val()) {
            localStorage.filename = $("#filename").val();
        }
    });

    initCommonObjects();


    if (localStorage.replSymb == undefined) {
        localStorage.replSymb = true;
    } else if (localStorage.replSymb == "false") {
        $("#replSymb").click();
    }

    if ($.getUrlVar('src')) {
        loadCode($.getUrlVar('src'))
        localStorage.code = getCode();
        history.pushState(null, "IDP web-IDE", "./");
    } else if (localStorage.code) {
        $("#filename").val(localStorage.filename);
        window.editor.mirror.setValue(localStorage.code);
    }


    $("#filename").on('input', function() {
        if (!$("#filename").val().endsWith(".idp")) {
            $("#filename").val($("#filename").val() + ".idp");
            $("#filename").setCursorPosition($("#filename").val().length - 4);
        }
    });

    if(isUpToDate()){
        $("#fileStatus").removeClass("glyphicon-flash");
        $("#fileStatus").addClass("glyphicon-ok");
    }

    setSize();
    myLayout.resizeAll();


    $("#abortButton").click(function() {
        killIDP();
    });
});
