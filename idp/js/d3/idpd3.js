/**
*	The global idpd3 object for animation.
*	This object holds the handlers and is responsible for the creation of Animation objects.
*/
var idpd3 = {
	/**
	*	The list of handlers to be called for animation.
	*/
	handlers : [],

	/**
	* The creator of a Animation object requires a svg-element to draw in.
	*/
	Animation : function(target) {
		this.target = target;
		this.scale = {
			type : "scale",
			x : d3.scale.linear(), 
			y : d3.scale.linear()
		}
		this.force = d3.layout.force()
		this.registerAnimation();
		this.setData(undefined);
		this.setIOHandler(undefined);
	},
	/**
	* Add a handler to the animation.
	*/
	addHandler : function (f) {
		this.handlers.push(f);
	},
	handleElements : function (animation) {
		for(i=0,x=this.handlers.length;i<x;i++){
			this.handlers[i](animation);
		}
	}
};

/**
* The prototype of animation object.
* This defines the methods available during drawing. 
* The public methods are setIOHandler(), setData(data), clearData(), nextTime()
*/
idpd3.Animation.prototype = {
	/**
	* Recalculates the minimum and maximum time, resets the time to the first time.
	*/
	resetTime : function(){
		try{
			console.log(this.data);
			var times = this.data.map(function(d) {return d.time; });
			this.minTime = Math.min.apply(Math, times);
			this.maxTime = Math.max.apply(Math, times);
			this.time = this.minTime;
		} catch(err) {
			console.log("Invalid idpd3 data, ignoring.");
			console.log(err);
		}
	},
	/**
	* Sets the callbackhandler for interactions.
	*/
	setIOHandler : function(io) {
		this.io = io || function() {};
	},
	/**
	* Returns the next time
	*/
	calcNextTime : function(i) {
		return (i+1 > this.maxTime ? this.minTime : i+1); 
	},
	/**
	* Changes the time and redraws the screen.
	*/
	nextTime : function() {
			this.time = this.calcNextTime(this.time);
			this.redraw();
	},
	/**
	* Redraws the screen
	*/
	redraw : function() {
		try{
			this.calcScale();
			idpd3.handleElements(this);
		} catch(err) {
			//console.log("Invalid idpd3 data, ignoring: " + err);
			throw err;
		}
	},
	/**
	* Unpacks the given object and uses the animation subobject.
	* Sets the data of the current animation and redraws the screen.
	*/
	setData : function(data) {
		this.data = data || this.data || [{hide:true, time:1, width:0, height:0, elements:[]}];
		//Convert array-like objects.
		this.data = $.map(this.data, function(value, key) {return value});
		this.resetTime();
		this.redraw();
	},
	/**
	* Removes the data from the animation. This also hides the drawing area.
	*/
	clearData : function() {
		this.data = undefined;
		this.setData(undefined);
	},
	/**
	* Registers a call to the drawing area to go to the next time-slice.
	*/
	registerAnimation : function() {
		var animation = this;
		this.target.on("click", function() { animation.nextTime(); });
	},
	/**
	* Returns the currrent timeslice, or hides when none is available.
	*/
	getData : function() {
		var time = this.time;
		var state = this.data.filter(function(d) { return d.time == time; })[0];
		state = state || {hide:true, time:1, width:0, height:0, elements:[]};
		return state;
	},
	/**
	* Calculates the scale to draw with. 
	* This makes drawings independent of the size of the svg-element.
	*/
	calcScale : function () {
		var base = this.getData();
		this.scale.x 
			.domain([0, base.width])
			.range([0, this.target.attr("width")]);
		this.scale.y
			.domain([0, base.height])
			.range([0, this.target.attr("height")]);
		this.scale.size = 0; 
		this.force.size([base.width, base.height])
	}
}

/**
* Adds a handler that hides the SVG-target when no animation is displayed.
*/
idpd3.addHandler(function (animation) {
	var hide = animation.getData().hide;
	var target = animation.target;
	if(hide){
		target.style('display', 'none');
	} else {
		target.style('display', 'inherit');
	}
});

var onclick = function(animation) {
		return function(d, i) {
			var key = d.key
			d3.event.stopPropagation();
			var time = animation.time;
			var e = {key : key, type : 'click'};
			var o = [{time : time, elements:[e]}];
			animation.io(o);
		}
}

idpd3.addHandler(function (animation) {
	var elements = animation.getData().elements;
	var target = animation.target;
	var scale = animation.scale;
	var links = elements.filter(function (d) { return d.type == "link";});
	var temp = target.selectAll("line")
		.data(links, function(d) {return d.key;});
	function updatePosition(d3El) {
			d3.select(d3El)
				.attr("x1", function (d) { return scale.x(+d.source.x); })
				.attr("y1", function (d) { return scale.y(+d.source.y); })
				.attr("x2", function (d) { return scale.x(+d.target.x); })
				.attr("y2", function (d) { return scale.y(+d.target.y); });
	}
	temp.exit()
			.transition()
			.style("stroke-opacity", function(d) { return 0; })
			.remove();
	temp.enter()
			.append("line")
			.classed("link", true)
			.style("stroke-opacity", function(d) { return 1; })
			.on("click", onclick(animation));
	temp.each(function (d) {
				d.updatePosition = updatePosition;
				d.source = elements.filter(function (d1) {
					return d1.key == d.link_from;})[0];
				d.target = elements.filter(function (d1) {
					return d1.key == d.link_to;})[0];
			});
	temp.transition()
			.duration(1000)
			.attr("x1", function (d) { return scale.x(+d.source.x); })
			.attr("y1", function (d) { return scale.y(+d.source.y); })
			.attr("x2", function (d) { return scale.x(+d.target.x); })
			.attr("y2", function (d) { return scale.y(+d.target.y); })
			.style("stroke-width", function (d) { return d.link_width || 1; })
			.style("stroke", function (d) { return d.color || "black"; });
});

/**
* Adds a handler for displaying rectangles.
*/
idpd3.addHandler(function (animation) {
	var elements = animation.getData().elements;
	var scale = animation.scale;
	var target = animation.target;
	var rectangles = elements.filter(function(d) { return d.type == "rect"; })
	var temp = target.selectAll("rect").data(rectangles, function(d) { return d.key; });
	function updatePosition(d3El) {
		d3.select(d3El)
			.attr("x", function (d) { return scale.x(+d.x - d.rect_width/2); })
			.attr("y", function (d) { return scale.y(+d.y - d.rect_height/2); });
	}
	temp.exit()
		.transition()
		.attr("x", function (d) { return scale.x(+d.x); })
		.attr("y", function (d) { return scale.y(+d.y); })
		.attr("width", 0)
		.attr("height", 0)
		.remove();
	var rect = temp.enter().append("rect")
		.attr("x", function (d) { return scale.x(+d.x); })
		.attr("y", function (d) { return scale.y(+d.y); })
		.attr("width", 0)
		.attr("height", 0)
		.style("fill", function(d) { return d.color; })
		.on("click", onclick(animation))
	temp
		.each(function (d) {
			d.updatePosition = updatePosition
			d.fixed = true;
			d.x = +d.x;
			d.y = +d.y;})
		.transition().duration(1000)
		.attr("x", function (d) { return scale.x(+d.x - d.rect_width/2); })
		.attr("y", function (d) { return scale.y(+d.y - d.rect_height/2); })
		.attr("width", function (d) { return scale.x(+d.rect_width); })
		.attr("height", function (d) { return scale.y(+d.rect_height); })
		.style("fill", function(d) { return d.color; })
		.each("end", function (d) {
			d.fixed = d.isFixed ||  false;
			animation.force.resume();
		});
});

idpd3.addHandler(function (animation, target) {
	var elements = animation.getData().elements;
	var scale = animation.scale;
	target = target || animation.target;
	var images = elements.filter(function (d) { return d.type == "img"; });
	var temp = target.selectAll("image").data(images, function (d) { return d.key; });
	function updatePosition(d3El) {
		d3.select(d3El)
			.attr("x", function (d) { return scale.x(+d.x - d.rect_width/2); })
			.attr("y", function (d) { return scale.y(+d.y - d.rect_height/2); });
	}
	temp.exit()
		.transition()
		.attr("x", function (d) { return scale.x(+d.x); })
		.attr("y", function (d) { return scale.y(+d.y); })
		.attr("width", 0)
		.attr("height", 0)
		.remove();
	var rect = temp.enter().append("svg:image")
		.attr("x", function (d) { return scale.x(+d.x); })
		.attr("y", function (d) { return scale.y(+d.y); })
		.attr("width", 0)
		.attr("height", 0)
		.style("fill", function(d) { return d.color; })
		.on("click", onclick(animation))
	temp
		.attr("xlink:href", function (d) { return d.img_path; })
		.each(function (d) {
			d.updatePosition = updatePosition
			d.fixed = true;
			d.x = +d.x;
			d.y = +d.y;})
		.transition().duration(1000)
		.attr("x", function (d) { return scale.x(+d.x - d.rect_width/2); })
		.attr("y", function (d) { return scale.y(+d.y - d.rect_width/2); })
		.attr("width", function (d) { return scale.x(+d.rect_width); })
		.attr("height", function (d) { return scale.y(+d.rect_height); })
		.style("fill", function(d) { return d.color; })
		.each("end", function (d) {
			d.fixed = d.isFixed ||  false;
			animation.force.resume();
		});
});

/**
* Adds a handler for displaying text.
*/
idpd3.addHandler(function (animation) {
	var data = animation.getData();
	var elements = data.elements;
	var scale = animation.scale;
	var target = animation.target;
	var texts = elements.filter(function(d) { return d.type == "text"; })
	var temp = target.selectAll("text").data(texts, function(d) { return d.key; });
	function updatePosition(d3El) {
		d3.select(d3El)
			.attr("x", function (d) { return scale.x(+d.x); })
			.attr("y", function (d) { return scale.y(+d.y); });
	}
	temp.exit()
		.transition()
		.attr("opacity", 0)
		.remove();
	var circ = temp.enter().append("text")
		.attr("text-anchor", "middle")
		.attr("opacity", 0)
		.attr("font-family", "sans-serif")
		.attr("font-size", function(d) { return scale.y(+d.text_size); })
		.attr("x", function (d) { return scale.x(+d.x); })
		.attr("y", function (d) { return scale.y(+d.y); })
		.text( function (d) { return d.text_label; })
		.style("fill", function(d) { return d.color; })
		.on("click", onclick(animation));
	temp.each(function (d) {
			d.updatePosition = updatePosition;
			d.fixed = true;
			d.x = +d.x;
			d.y = +d.y;})
		.transition().duration(1000)
		.text( function (d) { return d.text_label; })
		.attr("x", function (d) { return scale.x(+d.x); })
		.attr("y", function (d) { return scale.y(+d.y); })
		.attr("opacity", 1)
		.style("fill", function(d) { return d.color; })
		.each("end", function (d) {
			d.fixed = d.isFixed ||  false;
			animation.force.resume();
		});
});

/**
* Adds a handler for drawing circles.
*/
idpd3.addHandler(function (animation) {
	var elements = animation.getData().elements;
	var scale = animation.scale;
	var target = animation.target;
	var circles = elements.filter(function (d) { return d.type == "circ"; })
	function updatePosition(d3El) {
		d3.select(d3El)
			.attr("cx", function (d) { return scale.x(+d.x); })
			.attr("cy", function (d) { return scale.y(+d.y); });
	}
	var temp = target.selectAll("ellipse")
		.data(circles, function (d) { return d.key; });
	temp.exit()
		.each(function (d) {d.fixed = true})
		.transition()
		.attr("rx", 0)
		.attr("ry", 0)
		.remove();
	temp.enter()
		.append("ellipse")
		.attr("cx", function (d) { return scale.x(+d.x); })
		.attr("cy", function (d) { return scale.y(+d.y); })
		.attr("rx", 1)
		.attr("ry", 1)
		.style("fill", function (d) { return d.color; })
		.on("click", onclick(animation));
	temp.each(function (d) {
			d.updatePosition = updatePosition;
			d.fixed = true;
			d.x = +d.x;
			d.y = +d.y;})
		.transition().duration(1000)
		.attr("cx", function (d) { return scale.x(+d.x); })
		.attr("cy", function (d) { return scale.y(+d.y); })
		.attr("rx", function (d) { return scale.x(+d.circ_r);})
		.attr("ry", function (d) { return scale.y(+d.circ_r);})
		.style("fill", function (d) { return d.color; })
		.each("end", function (d) {
			d.fixed = d.isFixed ||  false;
			animation.force.resume();
		});
});

idpd3.addHandler(function (animation) {
	var target = animation.target;
	target.selectAll("*")
		.classed("node", function (d, i) {return d.node;});
})

idpd3.addHandler(function (animation) {
	var data = animation.getData();
	var elements = data.elements;
	var target = animation.target;
	var node = target.selectAll(".node");
	var link = target.selectAll(".link");
	var force = elements.filter(function (d) { return d.node;});
	var links = elements.filter(function (d) { return d.type == "link";});
	links = links.filter(
		function (d) {
			return force.indexOf(d.source) >= 0 
					&& force.indexOf(d.target) >= 0;
		});
	function updatePos(d) { if(d.updatePosition) {d.updatePosition(this);}}
	animation.force
		.nodes(force)
		.links(links)
		.on("tick", function(){
			link.each(updatePos);
		  node.each(updatePos);
		});
		animation.force.start();
		animation.force.stop();
});

idpd3.addHandler(function (animation) {
	var target = animation.target;
	target.selectAll("*").sort(
			function(a,b) {
				var ao = a.order || 0;
				var bo = b.order || 0;
				if(ao != bo) {
					console.log(ao+ " " + bo);
					return ao - bo;
				}
				ao = a.type == "link" ? -1 : 0;
				bo = b.type == "link" ? -1 : 0;
				if(ao != bo) {
					return ao - bo;
				}
				return d3.ascending(a.key, b.key);
			});
});

