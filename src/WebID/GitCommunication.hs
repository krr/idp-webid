{-# LANGUAGE ScopedTypeVariables #-}
module WebID.GitCommunication where

import Network.HTTP.Client 
import Network.HTTP.Types.Header
import Network.HTTP.Client.TLS

import qualified Data.ByteString as BS
import qualified Data.ByteString.Base64 as Base64

import Data.String.Conversions

import WebID.GitToken

gitLoadRequest :: String -> IO BS.ByteString
gitLoadRequest gistId =
    do
        mang <- newManager $ managerSetProxy defaultProxy tlsManagerSettings
        basicRequest <- parseUrl $ "https://api.github.com/gists/" ++ gistId
        modifiedRequest <- return $ basicRequest { method = convertString "GET", secure = True, requestHeaders = commonHeader}
        response <- httpLbs modifiedRequest mang
        return $ convertString $ responseBody response

gitSaveRequest :: String -> IO BS.ByteString
gitSaveRequest inputData =
    do
        mang <- newManager $ managerSetProxy defaultProxy tlsManagerSettings
        let body = RequestBodyBS $ convertString inputData
        basicRequest <- parseUrl "https://api.github.com/gists"
        modifiedRequest <- return basicRequest{ method = convertString "POST", secure = True, requestHeaders = commonHeader, requestBody = body}
        res <- httpLbs modifiedRequest mang
        return $ convertString $ responseBody res

getURL :: String -> IO BS.ByteString
getURL x = do
        mang <- newManager $ managerSetProxy defaultProxy tlsManagerSettings
        basicRequest <- parseUrl x
        response <- httpLbs basicRequest mang
        return $ convertString $ responseBody response


commonHeader :: [Header]
commonHeader = let authenticationHeader = (hAuthorization, convertString $ BS.append (convertString "Basic ") (Base64.encode $ convertString gitToken))
                   userAgentHeader = (hUserAgent, convertString "IDP-Client")
                     in [authenticationHeader,userAgentHeader]
