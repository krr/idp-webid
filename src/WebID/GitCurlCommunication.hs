{-# LANGUAGE ScopedTypeVariables #-}
module WebID.GitCurlCommunication where


import Data.ByteString (ByteString)

import Data.String.Conversions
import System.Process

import WebID.GitToken

gitLoadRequest :: String -> IO ByteString
gitLoadRequest gistId =
    do
        (_,out,_) <- readProcessWithExitCode "curl" ["-u",gitToken, "https://api.github.com/gists/"++gistId] ""
        return $ convertString out


gitSaveRequest :: String -> IO ByteString
gitSaveRequest inputData =
    do
        (_,out,_) <- readProcessWithExitCode "curl" ["-u",gitToken,"-X","POST","--data",inputData, "https://api.github.com/gists"] ""
        return $ convertString out

getURL :: String -> IO ByteString
getURL x = do
         (_,contents,_) <- readProcessWithExitCode "curl" [x] ""
         return $ convertString contents
