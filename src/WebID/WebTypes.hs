{-# LANGUAGE OverloadedStrings #-}
module WebID.WebTypes where

import Control.Applicative
import Control.Monad
import Data.Aeson

data EvalRequest = EReq {
        idpcode :: String,
        gv :: Int,
        sv :: Int,
        nb :: Int,
        inference :: Int,
        stable :: Bool,
        cp :: Bool,
        filename :: String,
        eInput :: String,
        idpV :: Int
      }
    deriving Show

instance FromJSON EvalRequest where
   parseJSON (Object v) = EReq    <$>
                          v .: "code" <*>
                          (maybe 0 id <$> (v .:? "GV")) <*>
                          (maybe 0 id <$> (v .:? "SV")) <*>
                          (maybe 0 id <$> (v .:? "NB")) <*>
                          v .: "inference" <*>
                          (maybe False id <$> (v .:? "stable")) <*>
                          (maybe True id <$> (v .:? "cp")) <*>
                          (maybe "tmp.idp" id <$> (v .:? "filename")) <*> 
                          (maybe "" id <$> (v .:? "eInput")) <*> 
                          (maybe 0 id <$> (v .:? "idpV"))

   parseJSON _          = mzero

data SimpleCode = Code String
    deriving Show

instance FromJSON SimpleCode where
  parseJSON (Object v) = Code    <$>
                         v .: "code"

  parseJSON _          = mzero

data EmptyRequest = NoRequest

instance FromJSON EmptyRequest where
   parseJSON _ = return NoRequest

data EvalResponse = ERes String String deriving Show
instance ToJSON EvalResponse where
   toJSON (ERes s s2) = object ["stdout" .= s, "stderr" .= s2]

data SaveRequest = SReq {
        scode :: String,
        spath :: String
      }
    deriving Show

instance FromJSON SaveRequest where
   parseJSON (Object v) = SReq    <$>
                          v .: "code" <*>
                          v .: "path"
   parseJSON _          = mzero

data GistSaveReq = GSR String String
instance ToJSON GistSaveReq where
  toJSON (GSR file vers) = object ["files" .= object 
                                      ["gistfile1.txt" .= object["content" .= file]
                                      ,"version.txt" .= object["content" .= vers]]]

maincode :: EvalRequest -> String
maincode e
      |inference e == 2 = "main()"
      |inference e == 3 = "print(\"\")"
      |inference e == 4 = ""
      |otherwise =
         "stdoptions.cpsupport = " ++ (if cp e then "true" else "false") ++ " " ++
         "stdoptions.nbmodels = " ++ show (nb e) ++ " " ++
         "stdoptions.verbosity.grounding = " ++ show (gv e) ++ " " ++
         "stdoptions.verbosity.solving = " ++ show (sv e) ++ " " ++
         "stdoptions.stablesemantics = " ++ (if stable e then "true" else "false") ++ " " ++
         case inference e of
            0 -> "printmodels(modelexpand(T,S))"
            1 -> "printmodels(minimize(T,S,O))"
            _ -> "print(\"unknown inference\")"
