{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DoAndIfThenElse #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import           Prelude                          hiding (mapM_, unlines)

import qualified Control.Concurrent.STM           as STM
import qualified Network.EngineIO.Snap            as EIOSnap
import qualified Network.SocketIO                 as SocketIO
import qualified Snap.Core                        as Snap
import qualified Snap.Http.Server                 as Snap
import qualified Snap.Util.FileServe              as Snap
import qualified Snap.Util.CORS                   as Snap
import           Control.Monad
import           Control.Monad.IO.Class           (liftIO)
import           Data.Aeson.Types
import qualified Data.Vector                      as Vector
import           Control.Applicative
import           Control.Monad.Trans.Reader       (ReaderT, ask, runReaderT)
import           Control.Monad.Trans.State.Strict (StateT)
import qualified Data.Aeson                       as Aeson

import           Control.Concurrent               (forkIO)
import qualified Data.ByteString                  as B
import           Data.Maybe                       (catMaybes)
import           Data.Text                        (Text, unlines, pack)
import           Data.Text.Encoding               (encodeUtf8)
import           Data.Text.IO                     (hGetLine)
import           System.Info                      (os)
import           System.IO                        (BufferMode (NoBuffering),
                                                   hClose, hIsEOF, hPutStr,
                                                   hSetBuffering, hGetContents, Handle,
                                                   hPutStrLn, hFlush)
import           System.Process
import           Data.Configurator
import           Data.Configurator.Types
import           Data.List                        (sort)
import           System.EasyFile                 (createDirectoryIfMissing,
                                                   doesDirectoryExist,
                                                   doesFileExist,
                                                   getDirectoryContents,
                                                   removeFile,
                                                   dropFileName,
                                                   takeFileName,
                                                   combine,
                                                   (</>),
                                                   takeExtension)
import           System.Environment.Executable

import           Data.String.Conversions
import           System.Exit
import           System.IO.Temp
import           System.Posix.Signals
import           System.Process.Internals
import           WebID.WebTypes
import           WebID.GitCommunication
import           Control.Exception
import           System.IO                  hiding (hGetLine)
import           Version                    (versionString)

data ServerConfig = ServerConfig {
    htmlPath :: String
  , examplePath :: String
  , workFolder :: String
  , port :: Int
  , endpoint :: String
  , timeLim :: Int
  , spaceLim :: Int
  , fileBrowse :: Bool
  , autoStart :: Bool
  , limit :: Bool
  , mode :: String
}

defaultServerConfig :: ServerConfig
defaultServerConfig = ServerConfig { htmlPath = "./idp/", examplePath = "./examples/", workFolder = ".", port = 4001,
  endpoint = "idp", timeLim = 5, spaceLim = 600, fileBrowse = False, autoStart = True, limit = True, mode = "server"}

updateFromConfig :: ServerConfig -> Config -> IO ServerConfig
updateFromConfig s@ServerConfig{..} conf = do
  htmlPath' <- lookupDefault htmlPath conf "htmlFolder" :: IO String
  examplePath' <- lookupDefault examplePath conf "exampleFolder" :: IO String
  workFolder' <- basePath conf :: IO String
  port' <- lookupDefault port conf "port" :: IO Int
  endpoint' <- ('/':) <$> lookupDefault endpoint conf "endpoint" :: IO String
  timeLim' <- lookupDefault timeLim conf "time-limit" :: IO Int
  spaceLim' <- lookupDefault spaceLim conf "space-limit" :: IO Int
  fileBrowse' <- lookupDefault fileBrowse conf "allow-file-browse" :: IO Bool
  autoStart'  <- lookupDefault autoStart conf "auto-start-browser" :: IO Bool
  return $ s { htmlPath = htmlPath', examplePath = examplePath', workFolder = workFolder', port = port',
  endpoint = endpoint', timeLim = timeLim', spaceLim = spaceLim', fileBrowse = fileBrowse',
  autoStart = autoStart', limit = (timeLim' > 0) || (spaceLim' > 0), mode = if fileBrowse' then "local" else "server"}

basePath :: Config -> IO String
basePath conf = do
      wf <- (++"/") <$> lookupDefault (error "No \"workfolder\" set") conf "workFolder"
      createDirectoryIfMissing True wf
      validWF <- doesDirectoryExist wf
      unless validWF (error (show wf ++ " does not exist or is not a directory"))
      return wf

workingPath :: IO FilePath
workingPath = fmap fst splitExecutablePath

main :: IO ()
main = do
  putStrLn $ "Welcome to the IDP IDE v." ++ versionString
  exePath <- workingPath
  (conf,_) <- autoReload autoConfig [Required (exePath++"webID.cfg")]
  serverConfig <- updateFromConfig defaultServerConfig conf
  idpCommandStr <- Data.Configurator.lookup conf "idpPath"
  idpCommandS <- fmap (fmap (combine exePath)) (lookupDefault ["No Path Given"] conf "idpPath" :: IO [FilePath])
  let idpCommands = maybe idpCommandS (:[]) idpCommandStr
  idpInstalled <- doesFileExist (head idpCommands)
  unless idpInstalled $ error ("Could not find IDP at the path: \""++head idpCommands++"\"")
  setupRoutes serverConfig idpCommands

setupRoutes :: ServerConfig -> [FilePath] -> IO ()
setupRoutes conf@ServerConfig{..} idpCommands = do
  let args = if limit then buildRunlimCommand idpCommands timeLim spaceLim else map (, []) idpCommands
  socketIoHandler <- SocketIO.initialize EIOSnap.snapAPI (socketServer conf args)
  let routes =
              [
                (convertString $ endpoint ++ "/socket", Snap.applyCORS Snap.defaultOptions socketIoHandler)
              , (convertString $ endpoint ++ "/eval" , Snap.applyCORS Snap.defaultOptions $ evalLocalIDP (map (, []) idpCommands) conf)
              , (convertString $ endpoint ++ "/examples", Snap.serveDirectory examplePath)
              , (convertString $ endpoint ++ "/getGist", Snap.applyCORS Snap.defaultOptions getGist)
              , (convertString $ endpoint ++ "/submitGist", Snap.applyCORS Snap.defaultOptions $ submitGist (head idpCommands))
              , (convertString $ endpoint ++ "/index.html", Snap.redirect' (if endpoint == "/" then "/" else convertString $ endpoint++"/") 302)
              , (convertString $ endpoint ++ "/", Snap.applyCORS Snap.defaultOptions $ Snap.serveDirectoryWith (Snap.defaultDirectoryConfig {Snap.indexFiles = [mode ++ ".html"]}) htmlPath)
              ]
              ++
              if endpoint == "/" then [] else
              [
                ("",  Snap.method Snap.GET $ Snap.redirect $ convertString $ endpoint ++ "/")
              , ("/", Snap.method Snap.GET $ Snap.redirect $ convertString $ endpoint ++ "/")
              ]
              ++
              if not fileBrowse then [] else
              [
               (convertString $ endpoint ++ "/listFiles", listFiles conf)
              , (convertString $ endpoint ++ "/getFile" , getFile conf)
              , (convertString $ endpoint ++ "/saveFile", saveFile conf)
              ]
  when autoStart $ openBrowser port
  let serve x = Snap.httpServe (Snap.setPort port Snap.defaultConfig) $ Snap.route x
  serve routes

listFiles :: ServerConfig -> Snap.Snap ()
listFiles conf = do
  let base = workFolder conf
  path <- convertString <$> Snap.readRequestBody 10000000
  c <- liftIO $ filter (/= "..") . filter (/= ".") . sort <$> getDirectoryContents (base </> path )
  Snap.writeText "<ul class=\"jqueryFileTree\" style=\"display: none;\">"
  mapM_ (\f -> do
      isFile <- liftIO $ doesFileExist (base </> path </> f)
      if (isFile && takeExtension f == ".idp") then
        void $ Snap.writeText $ convertString
              $ "<li class=\"file ext_"++takeExtension f ++"\"><a href=\"#\" rel=\""++(path</> f) ++"\">"++f++"</a></li>" else return ()
      isDir <- liftIO $ doesDirectoryExist (base </> path </> f)
      if isDir then
        void $ Snap.writeText $ convertString
              $ "<li class=\"directory collapsed\"><a href=\"#\" rel=\""++(path</> f) ++"/\">"++f++"</a></li>" else return ()
    ) c
  Snap.writeText "</ul>"

getIDPversion c = do
    (_,out,_) <- readProcessWithExitCodePath "/tmp/" c ["--version"] ""
    return out

buildRunlimCommand :: [FilePath] -> Int -> Int -> [(String,[FilePath])]
buildRunlimCommand idp time space =
  map (\x->("timeout", args ++ [x])) idp
  where args = ["--no-info-on-success", "-c", "-o", "/dev/null"]
                ++ concat (catMaybes (zipWith lessThanZero [time, space*1024] ["-t", "-m"]))
        lessThanZero :: Int -> String -> Maybe [FilePath]
        lessThanZero v s
          | v <= 0    = Nothing
          | otherwise = Just [s, convertString $ show v]

submitGist :: String -> Snap.Snap ()
submitGist idpCommand = do
  b <- Snap.readRequestBody 1000000
  vers <- liftIO $ getIDPversion idpCommand
  out <- liftIO $ gitSaveRequest $ convertString $ Aeson.encode (GSR (convertString b) vers)
  let temp = Aeson.decode $ convertString out
  let (Just o ) = temp
  let (Success ident) = parse (.: "id") o :: Result String
  Snap.writeText $ convertString ident

getGist :: Snap.Snap ()
getGist = do
  b <- Snap.readRequestBody 1000000
  out <- liftIO $ gitLoadRequest (init.tail.show $ b)
  let Just o = Aeson.decode $ convertString out
  let urlSource = ( .: "files") >=> (.: "gistfile1.txt") >=> (.: "raw_url")
  let (Success urlS) = parse urlSource o
  contentS <- liftIO . getURL $ urlS
  let urlVersion = ( .: "files") >=> (.: "version.txt") >=> (.: "raw_url")
  version <-
    case parse urlVersion o of
      (Success urlV) -> liftIO . getURL $ urlV
      _ -> return "3.4.0"
  writeJSON $ object ["source" .= (convertString contentS :: String),
                      "version" .= (convertString version :: String)]

getFile :: ServerConfig -> Snap.Snap ()
getFile conf = do
  let base = workFolder conf
  b <- Snap.readRequestBody 1000000
  r <- liftIO $ readFile $ base ++ convertString b
  Snap.writeText . convertString $ r

saveFile :: ServerConfig -> Snap.Snap ()
saveFile conf = do
  let base = workFolder conf
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  liftIO $ do
    e <- doesFileExist (base++spath d)
    when e $ removeFile (base ++ spath d)
    createDirectoryIfMissing True $ reverse $ dropWhile (/='/') $ reverse $ base++spath d
    writeFile (base ++ spath d) (scode d)
  Snap.writeText "Success"

evalLocalIDP :: [(String,[String])] -> ServerConfig -> Snap.Snap ()
evalLocalIDP exes conf = do
  Just d <- Aeson.decode <$> Snap.readRequestBody 1000000
  let (exe,args) = exes !! idpV d
  base <-fmap (++ dropFileName (filename d)) $ return $ workFolder conf
  out <- liftIO $ withTempFile  base "tmp.tidp" (\f h -> do
    hPutStr h (idpcode d)
    hClose h
    (_,out,err) <- readProcessWithExitCodePath base exe (args ++  [takeFileName f, "-e",maincode d]) (eInput d)
    return $ ERes out err)
  writeJSON out

readProcessWithExitCodePath
    :: String                   -- ^ Working Directory
    -> FilePath                 -- ^ Filename of the executable (see 'RawCommand' for details)
    -> [String]                 -- ^ any arguments
    -> String                   -- ^ stdinp
    -> IO (ExitCode,String,String) -- ^ exitcode, stdout, stderr
readProcessWithExitCodePath base cmd args input = do
    let cp_opts = (proc cmd args) {
                    std_in  = CreatePipe,
                    std_out = CreatePipe,
                    std_err = CreatePipe,
                    cwd = Just base
                  }
    (Just inh ,Just outh,Just errh,ph) <- createProcess cp_opts
    hPutStr inh input
    hClose inh
    out <- hGetContents outh
    err <- hGetContents errh
    -- wait on the process
    ex <- waitForProcess ph
    return (ex, out, err)

data RunningState = RS { procH :: ProcessHandle, inph :: Handle}

socketServer :: ServerConfig -> [(String,[String])] -> StateT SocketIO.RoutingTable (ReaderT SocketIO.Socket Snap.Snap) ()
socketServer conf exes = do
  curProcess <- liftIO $ STM.newTVarIO Nothing
  let killAndSwitch newVal = do {
      ph <- liftIO . STM.atomically . STM.swapTVar curProcess $ newVal;
      case ph of
        Nothing -> return ()
        Just p -> do
          liftIO $ killhandle (procH p)
          SocketIO.emit "stop"  ()
      }
  SocketIO.on "execute" $ \d -> do
        let (exe,args) = exes !!idpV d
        base <-fmap (++ dropFileName (filename d)) $ return $ workFolder conf
        (pathToFile,handle) <- liftIO $ openTempFile base "tmp.tidp"
        liftIO $ do
            hPutStr handle $ idpcode d
            hClose handle
        let cmd = args ++ [takeFileName pathToFile, "-e", maincode d] ++ ["-i" | inference d == 4]
        let cp_opts = (proc exe cmd) {
                std_in  = CreatePipe,
                std_out = CreatePipe,
                std_err = CreatePipe,
                cwd = Just base
              }
        (Just inh ,Just outh,Just errh,ph) <- liftIO $ createProcess cp_opts
        killAndSwitch (Just $ RS ph inh)
        SocketIO.emit "start" ()
        liftIO $ hSetBuffering outh NoBuffering
        liftIO $ hSetBuffering errh NoBuffering
        let readHandle s h = do
                 indic <- liftIO $ hIsEOF h
                 unless indic $ do
                    content <- liftIO $ unlines <$> readLineChunked h 100
                    SocketIO.emit s content
                    readHandle s h

        test <- ask :: ReaderT SocketIO.Socket IO SocketIO.Socket
        void . liftIO . forkIO $ runReaderT (readHandle "std-out" outh) test
        void . liftIO . forkIO $ runReaderT (readHandle "std-err" errh) test
        void . liftIO . forkIO $ flip runReaderT test $ do
                                            ex <- liftIO $ waitForProcess ph
                                            case ex of
                                              ExitSuccess -> killAndSwitch Nothing
                                              ExitFailure (-9) -> return ()
                                              ExitFailure 1 -> do
                                                    SocketIO.emit "std-err" ("ExitCode: 1" :: String)
                                                    SocketIO.emit "std-err" ("This can be caused by parsing errors, runtime errors or resource limits (cpu usage/ram)" :: String)
                                                    killAndSwitch Nothing
                                              ExitFailure i -> do
                                                    SocketIO.emit "std-err" ("ExitCode: "++show i)
                                                    killAndSwitch Nothing
                                            liftIO $ removeFile pathToFile

  SocketIO.on "abort" $ \NoRequest -> killAndSwitch Nothing
  SocketIO.on "std-in" $ \s -> do
        ph <- liftIO . STM.atomically . STM.readTVar $ curProcess;
        case ph of
          Just p -> liftIO $ do
            hPutStrLn (inph p) s
            hFlush (inph p)
          Nothing -> return ()
  SocketIO.appendDisconnectHandler (killAndSwitch Nothing)


writeJSON :: ToJSON a => a -> Snap.Snap ()
writeJSON a = do
  Snap.modifyResponse $ Snap.setHeader "Content-Type" "application/json"
  Snap.writeText. convertString . Aeson.encode $ a

openBrowser :: Int -> IO ()
openBrowser port = do
    putStrLn $ "Please open your browser and visit http://localhost:" ++ show port
    wp <- workingPath
    void $ case os of
      "darwin" -> runProcess "open" ["http://localhost:" ++ show port] Nothing Nothing Nothing Nothing Nothing
      "mingw32" -> runProcess (wp++"startbrowser.bat") [] Nothing Nothing Nothing Nothing Nothing
      _ -> runProcess "xdg-open" ["http://localhost:" ++ show port] Nothing Nothing Nothing Nothing Nothing


killhandle
    :: ProcessHandle    -- ^ A process in the process group
    -> IO ()
killhandle ph =
    withProcessHandle ph $
      \case
            ClosedHandle _ -> return ()
            OpenHandle h -> do
                signalProcess sigKILL h
                return ()

contentAvailable :: Handle -> IO Bool
contentAvailable h = either (const False) id <$> (try (hReady h) :: IO (Either IOException Bool))

readLineChunked :: Handle -> Int -> IO [Text]
readLineChunked h nbOfLines =
    if nbOfLines > 0 then do
        available <- contentAvailable h
        if available then do
            c  <- hGetLine h
            cs <- readLineChunked h (nbOfLines - 1)
            return $ c:cs
        else
            return []
    else
        return []
