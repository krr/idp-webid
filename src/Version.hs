{-# LANGUAGE TemplateHaskell #-}

module Version (version, versionString) where

import           Data.Version       (showVersion)
import           Development.GitRev (gitHash)
import qualified Paths_webID   as Paths


version :: String
version = showVersion Paths.version

versionString :: String
versionString = version ++ " [" ++ $(gitHash) ++ "]"
