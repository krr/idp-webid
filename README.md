IDP webID
========

IDP webID is a browser-based development environment for IDP.
The technology is a a combination of codemirror for the IDE features, bootstrap for the layout and JQuery for glueing it all together. A webserver binds IDP to the IDE.
All major platforms are supported, but only a linux pre-compiled executable is provided.

Requirements
-------------
1. `curl` for exporting files and loading exported files.
1. [Haskell Stack](https://docs.haskellstack.org/en/stable/README/)

Installation
-------------

1. Install IDP locally.
1. `clone bitbucket.org/krr/idp-webid && cd idp-webid`
1. Update the submodules by executing `git submodule init` and `git submodule update`. (**note**: If you don't have access to the `krr/idpd3` repo, remove it as a dependency in `.gitmodules` before executing following commands.)
1. Run `stack build` (this might take a while!)
1. Add the `timeout` dependency to the path: `export PATH=$PWD/dependencies/timeout:$PATH`
1. Run `stack install --local-bin-path .` to install the webID binary in the current working directory.
1. Edit `webID.cfg` to fit your needs.
1. Execute `./webID` and the webIDE should start running.
